FROM mcr.microsoft.com/dotnet/runtime:7.0-alpine

ADD publish /publish
ENTRYPOINT ["dotnet","/publish/dotnetcore.dll"]